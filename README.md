# cesar-hackathon-rcf

**Ce dépôt GIT contient le dump des données de la base CESAR :**
- au format `.sql` (dump de l'intégralité de la nouvelle version de la base).
- au format `.tsv` (un fichier par table).

*Ces données sont proposés selon les termes de la  Licence Creative Commons Attribution - Pas d’utilisation commerciale - Partage dans les mêmes conditions 4.0 International.* / *These data are made available under the Attribution-Non Commercial-ShareAlike 4.0 International Creative Commons Licence*


> :warning: Ces données sont issues de la base CESAR originale mais on fait l'objet d'un travail de réorganisation (renommage et restructuration des tables) afin de rendre leur manipulation plus aisée et permettre la création d'une nouvelle interface web sur http://cesar2.huma-num.fr/ . Ce travail étant toujours en cours l'intégralité des données n'est pas encore disponible ici.

> :warning: These data come from the original CESAR database but have been reorganised (tables were renamed and restructured) simplifying their handling and the creation of a new web interface (see http://cesar2.huma-num.fr/). As this is still an ongoing work not all original data are avaible here.  


## DUMP SQL

```
DEFAULT CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci
```

## DUMPS TSV (détail des tables)

<details> <summary markdown="span">List of tables</summary>

```
/tsv
    ├── activity.csv
    ├── anthology.csv
    ├── community_type.csv
    ├── company.csv
    ├── company_membership.csv
    ├── copy.csv
    ├── form.csv
    ├── language.csv
    ├── library.csv
    ├── location.csv
    ├── location_location_type.csv
    ├── location_type.csv
    ├── manuscript.csv
    ├── note.csv
    ├── performance.csv
    ├── performance_person.csv
    ├── person.csv
    ├── person_skill.csv
    ├── publication.csv
    ├── publication_location.csv
    ├── publisher.csv
    ├── publisher_membership.csv
    ├── qualifier.csv
    ├── script_creator.csv
    ├── script.csv
    ├── script_form.csv
    ├── script_language.csv
    ├── script_qualifier.csv
    ├── skill.csv
    └── social_title.csv

```
</details>


# Données de CESAR accessibles en ligne :

* Interface web sur http://cesar2.huma-num.fr/  (work in progress).

* Endpoint SPARQL sur https://cesar2.huma-num.fr/ avec requêtes pré-configurées et modifiables.
> L'Endpoint SPARQL permet également de rediriger vers des données de l'interface web graphique / des sorties en `json-ld`  

* API REST et documentation sur http://cesar2.huma-num.fr/api (sorties en `json`, `json-ld`, `txt`)

* Interface GRAPHQL sur http://cesar2.huma-num.fr/api/graphql/graphiql?
